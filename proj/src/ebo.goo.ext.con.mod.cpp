/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 on 2:45:19a, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is shared memory client desktop console application entry point file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:48:02p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack chrome extension console on 27-Sep-2019 at 1:32:31p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "StdAfx.h"

#ifndef __OUTPUT_TO_CONSOLE
#define __OUTPUT_TO_CONSOLE
#endif

#include "shared.gen.sys.com.h"
#include "shared.gen.con.iface.h"
#include "shared.gen.cmd.ln.h"

using namespace shared::sys_core;
using namespace shared::common  ;
using namespace shared::common::ui;

#include "ebo.goo.ext.install.h"
#include "ebo.goo.ext.down.dlg.h"
#include "ebo.goo.ext.runner.h"
#include "ebo.goo.ext.popups.h"
#include "ebo.goo.ext.crx.h"

using namespace ebo::out::ext;
using namespace ebo::out::ext::dialogs;

#include "shared.uix.gdi.provider.h"

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace out { namespace test {

	class CCon_Async : public CConsole, public ICrxCallback {
	private:
		virtual void       ICrx_OnUnpackFinish  (void  )    override sealed {
			this->WriteInfo(_T("Crx file archive extracting has finished;"));
		}
		virtual void       ICrx_OnUnpackProgress(const DWORD dwIndex, const DWORD dwTotal, CAtlString _file) override sealed {
			CAtlString cs_msg; dwIndex; dwTotal;
			cs_msg.Format (
				_T("Extracting file %s..."), _file.GetString()
			);
			this->WriteInfo(cs_msg.GetString());
		}
		virtual void       ICrx_OnUnpackStart   (void  )    override sealed {
			this->WriteInfo(_T("Crx file archive extracting has started;"));
		}
		virtual void       ICrx_OnUpdateError   (CError _err)    override sealed {
			this->WriteErr(_err);
		}
	};
}}}
using namespace ebo::out::test;
/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID) {
	INT n_res = 0;

	::SetConsoleOutputCP(CP_UTF8);

	CError sys_err;
	sys_err << __MODULE__ << S_OK >> __MODULE__;

	CCon_Async con_;
	CAtlString cap_;

	static LPCTSTR lp_sz_pat = _T("Ebo Pack Google Chrome Extension Console [%s]");

	cap_.Format  ( lp_sz_pat ,
#if defined(WIN64)
	#if _DEBUG
		_T("__x_64__debug__"  )
	#else
		_T("__x_64__release__")
	#endif
#else
	#if _DEBUG
		_T("__x_86__debug__"  )
	#else
		_T("__x_86__release__")
	#endif
#endif
	);
	con_.OnCreate((LPCTSTR)cap_);

	CCoIniter com_lib(false);
	if (com_lib == false) {
		con_.WriteErr(com_lib.Error());
		con_.OnClose ();
		return (n_res = 1);
	}

	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;

	CCommandLine cmd_ln;
	if (cmd_ln.Has(_T("exe_name")) == false) {
		con_.WriteWarn(_T("Command line argument 'exe_name' is not specified;"));
		con_.OnClose ();
		return (n_res = 1);
	}

#pragma region _sec
	CCoSecurityProvider sec_;
	HRESULT hr_ = sec_.InitNoIdentity();
	if (FAILED(hr_)) {
		con_.WriteErr(sec_.Error());
		con_.OnClose ();
		return (n_res = 1);
	}
#pragma endregion

	CAtlString cs_msg;
	CAtlString cs_exe_path;
	CAtlString cs_target;   // the path to target file that is created by copying/cloning command;

	CInstaller exe_inst;
	hr_ = exe_inst.Find(
			cmd_ln.Arg (_T("exe_name")).GetString(), cs_exe_path
	);
	if (FAILED(hr_)) {
		con_.WriteErr(exe_inst.Error());
		con_.OnClose ();
		return (n_res = 1); 
	}
	else {
		cs_msg.Format (
				_T("Installation path of '%s' is found:%s%s"), cmd_ln.Arg (_T("exe_name")).GetString(), lp_sz_sep, cs_exe_path.GetString()
			);
		con_.WriteInfo((LPCTSTR)cs_msg);
	}

	if (cmd_ln.Has(_T("copy_to"))) {
		const bool b_override = cmd_ln.Has(_T("override"));

		cs_target = cmd_ln.Arg(_T("copy_to"));

		cs_msg.Format(
			_T("Copying file:%s%s%sto%s%s"),
			lp_sz_sep, (LPCTSTR)cs_exe_path  , lp_sz_sep,
			lp_sz_sep, (LPCTSTR)cs_target
		);
		con_.WriteInfo((LPCTSTR)cs_msg);

		hr_ = exe_inst.Copy(
				(LPCTSTR)cs_exe_path, (LPCTSTR)cs_target, b_override
			);
		if (FAILED(hr_)) {
			con_.WriteErr(exe_inst.Error());
		}
		else {
			con_.WriteInfo(_T("Copying file is succeeded;"));
		}
	}

	if (cmd_ln.Has(_T("clone_to"))) {
		const bool b_override = cmd_ln.Has(_T("override"));

		cs_target.Format(
			_T("%s\\%s"), exe_inst.Info().Path().GetString(), cmd_ln.Arg(_T("clone_to")).GetString()
		);

		cs_msg.Format(
			_T("Cloning file:%s%s%sto%s%s"),
			lp_sz_sep, (LPCTSTR)cs_exe_path  , lp_sz_sep,
			lp_sz_sep, (LPCTSTR)cs_target
		);
		con_.WriteInfo((LPCTSTR)cs_msg);

		hr_ = exe_inst.Copy(
			(LPCTSTR)cs_exe_path, (LPCTSTR)cs_target, b_override
		);
		if (FAILED(hr_)) {
			con_.WriteErr(exe_inst.Error());
		}
		else {
			con_.WriteInfo(_T("Cloning file is succeeded;"));
		}
	}

	if (cs_target.IsEmpty()) // no copying and cloning;
		cs_target = cs_exe_path;

	CAtlString cs_ext_src;
	CAtlString cs_ext_crx;

	if (cmd_ln.Has(_T("load_from")) &&
	    cmd_ln.Has(_T("save_to"))) {

		con_.WriteInfo(_T("Downloading data has started..."));

		cs_ext_src = cmd_ln.Arg(_T("load_from"));
		cs_ext_crx = cmd_ln.Arg(_T("save_to"));

		CDownloadDlg dlg_(cs_ext_src, cs_ext_crx);
		hr_ = dlg_.DoModal(::GetConsoleWindow());
		if (dlg_.Error().Is()) {
			con_.WriteErr(dlg_.Error());
		}
		else {
			cs_msg.Format(
				_T("Downloading file:%s%s%sto%s%s%shas completed;"),
				lp_sz_sep, (LPCTSTR)cs_ext_src  , lp_sz_sep,
				lp_sz_sep, (LPCTSTR)cs_ext_crx  , lp_sz_sep
			);
			con_.WriteInfo((LPCTSTR)cs_msg);
		}
	}

	CAtlString cs_zip;

	if (cmd_ln.Has(_T("unpack" ))) cs_zip     = cmd_ln.Arg(_T("unpack" ));
	if (cmd_ln.Has(_T("save_to"))) cs_ext_crx = cmd_ln.Arg(_T("save_to"));

	if (cmd_ln.Has(_T("unpack" ))) {
		CCrx  crx_;
		hr_ = crx_.Unpack((LPCTSTR)cs_zip, (LPCTSTR)cs_ext_crx, con_);
		if (FAILED(hr_)) {
			sys_err = crx_.Error();
			con_.WriteErr(sys_err);
		}
	}

	if (cmd_ln.Has(_T("apply_crx"))) {
		CRunner runner;
		runner.Args((LPCTSTR) cs_ext_crx);

		cs_msg.Format(
			_T(
				"Running target app: %s"
				"path = %s%s"
				"args = %s%s"
			),  lp_sz_sep, (LPCTSTR) cs_target, lp_sz_sep, runner.Args(), lp_sz_sep
		);

		con_.WriteInfo(cs_msg);
		//
		// we need to use the installation structure because chrome keeps its process name internally;
		// it does not matter how file is renamed or copied, the process name is the same;
		// thus, for finding a running chrome instance, original name is used;
		//
		hr_ = runner.Run((LPCTSTR) cs_target, runner.Args());
		if (FAILED(hr_)) {
			con_.WriteErr(runner.Error());
		}
		else {
			con_.WriteInfo(_T("Running the app is successful;"));
		}

		if (cmd_ln.Has(_T("popup")) && runner.Error().Is() == false) {

			CAtlString cs_popup = cmd_ln.Arg(_T("popup"));
			cs_msg.Format(
				_T("Looking for popup '%s' and trying to close it;"),  (LPCTSTR)cs_popup
			);
			con_.WriteInfo(cs_msg);

			const DWORD delay_ms = static_cast<DWORD>(cmd_ln.Arg(_T("wait_for"), 5 * 1000)); // default 5 sec (in milliseconds)

			CPopup_Async pop_as;
			hr_ = pop_as.Suppress((LPCTSTR)cs_popup, exe_inst.Info(), delay_ms);
			if (FAILED(hr_))
				con_.WriteErr(pop_as.Error());
			else {
				con_.WriteInfo(_T("Looking for displaying extensions..."));
				while (pop_as.IsRunning()) {
					::Sleep(10);
				}
			}
			if (SUCCEEDED(hr_)) {
				if (S_FALSE == hr_)
					con_.WriteWarn(pop_as.Error().Desc());
				else
					con_.WriteInfo(_T("Specified popup is closed;"));
			}
		}
	}
	con_.OnClose();

	return n_res;
}