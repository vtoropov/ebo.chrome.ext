#ifndef _STDAFX_H_A2AD1503_5554_ABCD_9B38_341E6E697B7E_INCLUDED
#define _STDAFX_H_A2AD1503_5554_ABCD_9B38_341E6E697B7E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:17:57p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console project on 5-Sep-2019 at 3:07:06p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack chrome extension console project on 27-Sep-2019 at 1:16:12p, UTC+7, Novosibirsk, Tulenina, Friday;
*/
#include "ebo.goo.ext.con.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of 'abcd' hides class member (GDI+)

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#include <atlbase.h>
#include <atlcom.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlctrls.h>

#include <comdef.h>
#include <atlstr.h>
#include <atlsafe.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>
#include <time.h>
#include <ctime>
#include <typeinfo>

#if (0)
#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#pragma comment(lib, "proj.shared.proc_v15.lib")
#pragma comment(lib, "proj.shared.temp_v15.lib")
#pragma comment(lib, "proj.shared.xzip_v15.lib")

#endif/*_STDAFX_H_A2AD1503_5554_ABCD_9B38_341E6E697B7E_INCLUDED*/