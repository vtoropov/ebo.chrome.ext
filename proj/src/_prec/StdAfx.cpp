/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:19:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:10:17p, UTC+7, Novosibirsk, Tulenina, Thursday;
	Adopted to Ebo Pack chrome extension console on 27-Sep-2019 at 1:19:34p, UTC+7, Novosibirsk, Tulenina, Friday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)