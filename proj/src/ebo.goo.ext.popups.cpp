/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Oct-2019 at 7:20:53p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack shared library Google Chrome popup message handler interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.goo.ext.popups.h"

using namespace ebo::out::ext;

#include "shared.gen.sys.com.h"
#include "shared.gen.event.h"

using namespace shared::sys_core;
using namespace shared::runnable;

/////////////////////////////////////////////////////////////////////////////

CFrame:: CFrame(const HWND h_wnd) { *this = h_wnd; }
CFrame::~CFrame(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFrame::Close(void) {
	if (NULL == m_wnd || m_wnd.IsWindow() == FALSE)
		return OLE_E_INVALIDHWND;
	m_wnd.SendMessage(WM_CLOSE) ;
	return S_OK;
}
const bool CFrame::Match(LPCTSTR _lp_sz_cap) const {
	bool bIs_matched = false;
	if (NULL == m_wnd || m_wnd.IsWindow() == FALSE)
		return  bIs_matched;
	if (NULL == _lp_sz_cap || 0 == ::_tcslen(_lp_sz_cap))
		return  bIs_matched;

	CAtlString  cs_cap;
	m_wnd.GetWindowText(cs_cap);
	if (cs_cap.IsEmpty())
		return  bIs_matched;

	const INT n_pos = cs_cap.Find(_lp_sz_cap);
	if (-1 != n_pos)
		return (bIs_matched = true);
	else
		return (bIs_matched = (0 == cs_cap.CompareNoCase(_lp_sz_cap)));
}
const
CWindow&   CFrame::Window(void) const { return m_wnd; }
CWindow&   CFrame::Window(void)       { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

CFrame&    CFrame::operator = (const HWND _h_wnd) { m_wnd = _h_wnd; return *this; }

/////////////////////////////////////////////////////////////////////////////

CFrames:: CFrames(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CFrames::~CFrames(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CFrames::Append (const TWindows& _handles) {
	m_error << __MODULE__ << S_OK;

	for (size_t i_ = 0; i_ < _handles.size(); i_++) {
		const CWindow& wnd =  _handles[i_];
		if ( NULL   == wnd || wnd.IsWindow() == FALSE)
			continue;
		try {
			CFrame frame_(wnd);
			m_frames.push_back(frame_);
		}
		catch (const ::std::bad_alloc&) {
			m_error = E_OUTOFMEMORY; break;
		}
	}
	if (m_error.Is()) {
		bool b_break = false; b_break = !b_break;
	}

	return m_error;
}

HRESULT     CFrames::Close  (const INT _n_ndx) {
	m_error << __MODULE__ << S_OK;

	if (_n_ndx < 0 || _n_ndx > static_cast<INT>(m_frames.size()) - 1)
		return (m_error = DISP_E_BADINDEX);

	CFrame& frm_ = m_frames[_n_ndx]; frm_.Close();
	m_frames.erase(m_frames.begin() + _n_ndx);

	return m_error;
}

HRESULT     CFrames::Create (const TProcessList& _processes) {
	m_error << __MODULE__ << S_OK;

	if (m_frames.empty() == false)
		m_frames.clear();

	for (size_t i_ = 0; i_ < _processes.size(); i_++) {
		const CProcess& proc_ =  _processes[i_];
		HRESULT hr_ = this->Append(proc_.Windows());
		if (FAILED(hr_))
			break;
	}

	return m_error;
}

TErrorRef   CFrames::Error  (void) const { return m_error; }

INT         CFrames::IsFound(LPCTSTR lp_sz_cap) const {
	m_error << __MODULE__ << S_OK;
	INT n_ndx = -1;

	if (m_frames.empty() == true) {
		((m_error = S_FALSE) = _T("No popup is found;"));
		return n_ndx;
	}

	for (size_t i_ = 0; i_ < m_frames.size(); i_++) {
		const CFrame& frm_ = m_frames[i_];
		if (frm_.Match(lp_sz_cap)) {
			n_ndx = static_cast<INT>(i_);
			break;
		}
	}

	if (-1 == n_ndx) {
		CAtlString cs_msg; cs_msg.Format(
			_T("Popup window '%s' is not found;"), lp_sz_cap
		);
		(m_error = S_FALSE) = cs_msg.GetString();
	}

	return n_ndx;
}

HRESULT     CFrames::Update (LPCTSTR lp_sz_exe) {
	m_error << __MODULE__ << S_OK;
	if (NULL == lp_sz_exe || 0 == ::_tcslen(lp_sz_exe))
		return ((m_error = E_INVALIDARG) = _T("Executable name is empty or invalid"));

	TProcessList  runs_;
	CProcLookup   proc_look; runs_ = proc_look.Find(lp_sz_exe, true);
	if (proc_look.Error().Is()) {
		m_error = proc_look.Error();
		return m_error;
	}

	*this << runs_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CFrames&    CFrames::operator+=(const TWindows& _handles) { this->Append(_handles); return *this; }
CFrames&    CFrames::operator<<(const TProcessList& _procs) { this->Create(_procs); return *this; }

/////////////////////////////////////////////////////////////////////////////

CPopups:: CPopups(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPopups::~CPopups(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CPopups::Block (const INT _n_ndx ) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_frames.Close(_n_ndx);
	if (FAILED(hr_))
		m_error = m_frames.Error();

	return m_error;
}

HRESULT   CPopups::Block (LPCTSTR lp_sz_cap) {
	m_error << __MODULE__ << S_OK;

	const INT n_ndx = m_frames.IsFound(lp_sz_cap);
	if (-1 == n_ndx)
		return (m_error = m_frames.Error());
	else
		return this->Block(n_ndx);
}
TErrorRef CPopups::Error (void) const { return m_error ; }
const
CFrames&  CPopups::Frames(void) const { return m_frames; }
CFrames&  CPopups::Frames(void)       { return m_frames; }

/////////////////////////////////////////////////////////////////////////////

CPopup_Async:: CPopup_Async(void) : m_msec(0) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CPopup_Async::~CPopup_Async(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CPopup_Async::Suppress (LPCTSTR lp_sz_cap, const CInst_Info& _info, const DWORD _wait) {
	m_error << __MODULE__ << S_OK;

	if (NULL == lp_sz_cap || 0 == ::_tcslen(lp_sz_cap))
		return ((m_error = E_INVALIDARG) = _T("Popup caption is empty or invalid;"));

	if (_info.Name().IsEmpty())
		return ((m_error = E_INVALIDARG) = _T("Executable name is not specified;"));

	if (0 == _wait)
		return ((m_error = E_INVALIDARG) = _T("Waiting period is not valid or too small;"));

	if (TThread::IsRunning())
		return ((m_error = __DwordToHresult(ERROR_INVALID_STATE)) = _T("Thread is already running;"));

	m_info      = _info;
	m_suppress  = lp_sz_cap;
	m_msec      = _wait;

	HRESULT hr_ = TThread::Start();
	if (FAILED(hr_))
		(m_error = hr_) = _T("Cannot start popup monitor;");

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

VOID      CPopup_Async::ThreadFunction(VOID) {

	CCoIniter com_core(false);
	if (com_core.IsSuccess() == false){
		m_error = com_core.Error();
		return;
	}

	HRESULT hr_ = m_error;

	CWaitCounter wait_(100, this->m_msec);

	while (TThread::m_crt.IsStopped() == false && SUCCEEDED(hr_)) {
		wait_.Wait();

		hr_ = this->Frames().Update(m_info.Name());
		if (FAILED(hr_)){
			m_error = this->Frames().Error();
			break;
		}
		const INT n_ndx = this->Frames().IsFound(this->m_suppress);
		if (-1 != n_ndx) {
			hr_ = this->Block(n_ndx);
		}
		if (FAILED(hr_))
			break;

		if (wait_.IsElapsed() == false) {
			continue;
		}
		else
			break;
	}
	::SetEvent(TThread::m_crt.EventObject());
	TThread::m_state = CThreadState::eStopped;
}