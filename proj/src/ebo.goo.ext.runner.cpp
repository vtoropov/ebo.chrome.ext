/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Oct-2019 at 10:10:38p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack shared library generic application runner interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.goo.ext.runner.h"

using namespace ebo::out::ext;

#include "shared.gen.sys.com.h"
#include "shared.gen.event.h"

using namespace shared::sys_core;
using namespace shared::runnable;
/////////////////////////////////////////////////////////////////////////////

CRunner:: CRunner(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CRunner::~CRunner(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCTSTR     CRunner::Args   (void) const { return m_args.GetString(); }
VOID        CRunner::Args   (LPCTSTR _lp_sz_crx) {
	// https://peter.sh/experiments/chromium-command-line-switches/
	m_args.Format(
		_T("--load-extension %s"), _lp_sz_crx
	);
}

TErrorRef   CRunner::Error  (void) const { return m_error ; }

HRESULT     CRunner::Run    (const CInst_Info& _info) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = this->Run(_info.Target(), (LPCTSTR)m_args);
	if (FAILED(hr_))
		return hr_;
#if (0)
	const bool bIs_Running = this->Running(_info.Name().GetString());
	if (bIs_Running) {
		CAtlString cs_desc;
		           cs_desc.Format(
			_T("The process '%s' is already running;") , _info.Name().GetString()
		);
		return ((m_error = __DwordToHresult(ERROR_OBJECT_ALREADY_EXISTS)) = (LPCTSTR)cs_desc);
	}
#endif
	return m_error;
}

HRESULT     CRunner::Run    (LPCTSTR _lp_sz_exe) {
	m_error << __MODULE__ << S_OK;

	const bool bIs_Running = this->Running(_lp_sz_exe);
	if (bIs_Running) {
		return (m_error = __DwordToHresult(ERROR_OBJECT_ALREADY_EXISTS)) = _T("The application is already running;");
	}

	return this->Run(_lp_sz_exe, (LPCTSTR)m_args);
}

HRESULT     CRunner::Run    (LPCTSTR _lp_sz_exe, LPCTSTR _lp_sz_crx) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_exe || 0 == ::_tcslen(_lp_sz_exe)) {
		m_error.State().Set(
			E_INVALIDARG, _T("Target app path is empty or invalid;")
		);
		return m_error; 
	}

	if (NULL == _lp_sz_crx || 0 == ::_tcslen(_lp_sz_crx)) {
		m_error.State().Set(
			E_INVALIDARG, _T("Chrome extension path is empty or invalid;")
		);
		return m_error; 
	}

	SHELLEXECUTEINFO sh_info = {0};
	sh_info.cbSize           = sizeof(SHELLEXECUTEINFO);
	sh_info.fMask            = SEE_MASK_DEFAULT;
	sh_info.hwnd             = HWND_DESKTOP ;
	sh_info.lpFile           = _lp_sz_exe   ;
	sh_info.lpParameters     = m_args.GetString();
	sh_info.lpVerb           = _T("open")   ;
	sh_info.nShow            = SW_SHOWNORMAL;

	HRESULT hr_ = ::ShellExecuteEx(&sh_info);
	if (FAILED(hr_))
		m_error = __LastErrToHresult();
	
	return m_error;
}

bool        CRunner::Running(LPCTSTR _lp_sz_exe) const {
	m_error << __MODULE__ << S_OK;

	bool bIs_found = false;

	if (NULL == _lp_sz_exe || 0 == ::_tcslen(_lp_sz_exe)) {
		(m_error = E_INVALIDARG) = _T("Executable name is empty or invalid;");
		return bIs_found;
	}

	TProcessList  runs_;
	CProcLookup   proc_look; runs_ = proc_look.Find(_lp_sz_exe, true);
	if (proc_look.Error().Is()) {
		m_error = proc_look.Error();
		return bIs_found;
	}

	return (bIs_found = !runs_.empty());
}