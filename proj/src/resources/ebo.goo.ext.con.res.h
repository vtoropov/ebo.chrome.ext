//
//   Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:21:36p, UTC+7, Phuket, Rawai, Tuesday;
//   This is communication data exchange receiver desktop console application resource declaration file.
//   -----------------------------------------------------------------------------
//   Adopted to Ebo Pack virtual printer console on 5-Sep-2019 at 3:14:50p, UTC+7, Novosibirsk, Tulenina, Thursday;
//   Adopted to Ebo Pack chrome extension console on 27-Sep-2019 at 1:21:32p, UTC+7, Novosibirsk, Tulenina, Friday;
//

#pragma region __generic

#define IDR_EBO_GOO_EXT_CON_ICO       1001

#define IDS_EBO_GOO_EXT_PRO_ADD       1101
#define IDS_EBO_GOO_EXT_PRO_CHK       1103
#define IDS_EBO_GOO_EXT_PRO_REM       1105
#define IDS_EBO_GOO_EXT_PRO_CPY       1107
#define IDS_EBO_GOO_EXT_PRO_ENU       1109

#pragma endregion

#pragma region down_dlg

#define IDD_EBO_GOO_EXT_DOWN_DLG      1201
#define IDC_EBO_GOO_EXT_DOWN_LAB      1203
#define IDC_EBO_GOO_EXT_DOWN_PRG      1205
#define IDC_EBO_GOO_EXT_DOWN_AVA      1207
#define IDR_EBO_GOO_EXT_DOWN_EXC      1209
#define IDR_EBO_GOO_EXT_DOWN_INF      1211
#define IDR_EBO_GOO_EXT_DOWN_HRS      1213

#pragma endregion



