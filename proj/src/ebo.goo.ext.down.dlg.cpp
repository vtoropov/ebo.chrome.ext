/*
	Created by Tech_Dog (ebontrop@gmail.com) on 11-Apr-2017 at 3:47:25a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian installer download UI dialog class implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google Chrome extension project on 6-Oct-2019 at 1:25:28a, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "StdAfx.h"
#include "ebo.goo.ext.down.dlg.h"
#include "ebo.goo.ext.con.res.h"

using namespace shared::net;
using namespace ebo::out::ext::dialogs;

#include "shared.gen.app.res.h"

using namespace shared::user32;

#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.provider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace out { namespace ext { namespace dialogs { namespace _impl {

	HRESULT  CDownloadDlg_AdjustImg (const HWND _dlg, const WORD _ctrl, const WORD _resId) {

		HBITMAP hBitmap = NULL;
		HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
			_resId,
			NULL  ,
			hBitmap
		);
		if (SUCCEEDED(hr_))
		{
			::WTL::CStatic ctrl_ = CWindow(_dlg).GetDlgItem(_ctrl);
			if (ctrl_)
				ctrl_.SetBitmap(hBitmap);

			::DeleteObject(hBitmap);
			hBitmap = NULL;
		}

		return (hr_ = S_OK);
	}

}}}}}

using namespace ebo::out::ext::dialogs::_impl;
/////////////////////////////////////////////////////////////////////////////

CDownloadDlg::CDownloadDlgImpl::CDownloadDlgImpl(const CAtlString& _source, const CAtlString& _target): 
	IDD(IDD_EBO_GOO_EXT_DOWN_DLG),
	m_source(_source),
	m_target(_target),
	m_timer (NULL)   ,
	m_interrupted(false),
	m_loader(*this)
{
	m_error << __MODULE__ << S_OK >> __MODULE__;
}

CDownloadDlg::CDownloadDlgImpl::~CDownloadDlgImpl(void) { }

/////////////////////////////////////////////////////////////////////////////

LRESULT CDownloadDlg::CDownloadDlgImpl::OnDestroy(UINT  uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (m_loader.IsRunning())
		m_loader.Stop();

	m_progress = NULL;
	m_label    = NULL;

	if (m_timer)
		TBaseDlg::KillTimer(m_timer); m_timer = NULL;

	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnDismiss(WORD, WORD, HWND hControl, BOOL& bHandled)
{
	bHandled = TRUE;
	m_interrupted = true;

	if (m_loader.IsRunning())
		m_loader.MarkToStop();

	CWindow cancel_button = hControl;
	cancel_button.EnableWindow(FALSE);

	m_timer = TBaseDlg::SetTimer(1, 500);
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	TBaseDlg::CenterWindow();
	{
		CApplicationIconLoader loader_(IDR_EBO_GOO_EXT_CON_ICO);
		TBaseDlg::SetIcon(loader_.DetachLargeIcon(), TRUE );
		TBaseDlg::SetIcon(loader_.DetachSmallIcon(), FALSE);
	}

	CDownloadDlg_AdjustImg(*this, IDC_EBO_GOO_EXT_DOWN_AVA, IDR_EBO_GOO_EXT_DOWN_HRS);

	m_progress = TBaseDlg::GetDlgItem(IDC_EBO_GOO_EXT_DOWN_PRG);
	m_label    = TBaseDlg::GetDlgItem(IDC_EBO_GOO_EXT_DOWN_LAB);
	m_loader.SourceURL (m_source);
	m_loader.TargetFile(m_target);

	HRESULT hr_ = m_loader.Start();
	if (FAILED(hr_)) {
		m_error = m_loader.Error();
		m_label.SetWindowText(m_error.Desc());
		CDownloadDlg_AdjustImg(*this, IDC_EBO_GOO_EXT_DOWN_AVA, IDR_EBO_GOO_EXT_DOWN_EXC);
	}
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			CWindow cancel_button = TBaseDlg::GetDlgItem(IDCANCEL);
			this->OnDismiss(0, 0, cancel_button, bHandled);
		} break;
	}
	return 0;
}

LRESULT CDownloadDlg::CDownloadDlgImpl::OnTimerEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	if (m_loader.IsComplete() ||
		m_loader.Error().Is() || m_error.Is())
	{
		TBaseDlg::KillTimer(m_timer);
		m_timer = NULL;
		TBaseDlg::EndDialog(IDOK);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

bool    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnCanContinue (void) { return !m_interrupted; }
void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnBegin (void) {
	if (m_progress)
		m_progress.SetRange(1, 100);
	CDownloadDlg_AdjustImg(*this, IDC_EBO_GOO_EXT_DOWN_AVA, IDR_EBO_GOO_EXT_DOWN_INF);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnFinish(void) {
	m_error = S_OK;
	m_timer = TBaseDlg::SetTimer(1, 500);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal)
{
	if (dwTotal < 1)
		return;

	static INT prior_ = 0;
	INT curr_ = INT(FLOAT(dwReceived)/FLOAT(dwTotal) * 100.0);
	if (prior_ != curr_)
	{
		prior_  = curr_;
		if (m_label)
		{
			CAtlString cs_value;
			cs_value.Format(
				_T("Downloading: %d %%"), curr_
				);
			m_label.SetWindowText(cs_value);
		}
		if (m_progress)
			m_progress.SetPos(curr_);
	}
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnError (CError _error) {
	m_error = _error;
	m_timer = TBaseDlg::SetTimer(1, 500);
}

void    CDownloadDlg::CDownloadDlgImpl::INetDownloader_OnInterrupt(void) {
	CAtlString cs_msg(_T("Downloading has been interrupted;"));
	m_error.State().Set(
			(DWORD)ERROR_CANCELLED,
			cs_msg
		);
}

/////////////////////////////////////////////////////////////////////////////

CDownloadDlg:: CDownloadDlg(const CAtlString& _source, const CAtlString& _target) : m_dlg(_source, _target) { }
CDownloadDlg::~CDownloadDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDownloadDlg::DoModal(const HWND hParent) {
	m_dlg.DoModal(hParent);
	return  this->Error();
}

TErrorRef  CDownloadDlg::Error(void)const { return m_dlg.m_error; }