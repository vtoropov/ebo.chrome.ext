/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Oct-2019 at 9:32:44p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack shared library generic dynamic binding interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.goo.ext.bind.h"

using namespace ebo::out::ext;

namespace ebo { namespace out { namespace ext {

	LPCTSTR bind_token_to_lib(const e_lib_token _token) {

		switch (_token) {
		case e_lib_token::e_advapi: { static LPCTSTR lp_sz_lib = _T("advapi32" ); return lp_sz_lib; } break;
		case e_lib_token::e_helper: { static LPCTSTR lp_sz_lib = _T("Iphplapi" ); return lp_sz_lib; } break;
		case e_lib_token::e_kernel: { static LPCTSTR lp_sz_lib = _T("kernel32" ); return lp_sz_lib; } break;
		case e_lib_token::e_shell : { static LPCTSTR lp_sz_lib = _T("shell32"  ); return lp_sz_lib; } break;
		case e_lib_token::e_socket: { static LPCTSTR lp_sz_lib = _T("mswsock"  ); return lp_sz_lib; } break;
		case e_lib_token::e_user32: { static LPCTSTR lp_sz_lib = _T("ntdll.dll"); return lp_sz_lib; } break;
		case e_lib_token::e_ws2_32: { static LPCTSTR lp_sz_lib = _T("ws2_32"   ); return lp_sz_lib; } break;
		}
		return NULL;
	}

	FARPROC bind_load_dll_func(const e_lib_token _token, LPCSTR _fn_name, const INT _ordinal) {

		static HMODULE lib_handle[] = {
			NULL, NULL, NULL, NULL, NULL, NULL, NULL
			};
		if (lib_handle[_token] == NULL) {
			lib_handle[_token] = ::LoadLibrary(bind_token_to_lib(_token));
		}
		if (lib_handle[_token] == NULL) {
			return NULL;
		}
		if (_ordinal) {
			return ::GetProcAddress(lib_handle[_token], (LPCSTR)MAKEINTRESOURCEA(_ordinal));
		}
		else {
			return ::GetProcAddress(lib_handle[_token], _fn_name);
		}
	}

}}}