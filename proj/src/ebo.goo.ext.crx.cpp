/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Oct-2019 at 5:05:00p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack shared library generic CRX wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.goo.ext.crx.h"

using namespace ebo::out::ext;

#include "shared.fs.gen.folder.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

CCrx:: CCrx (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CCrx::~CCrx (void) {} 

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CCrx::Error  (void) const { return m_error; }
HRESULT      CCrx::Unpack (LPCTSTR _lp_sz_zip, LPCTSTR _lp_sz_dest, ICrxCallback& _sink) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_zip  || 0 == ::_tcslen(_lp_sz_zip )) return ((m_error = E_INVALIDARG) = _T("Zip file path is empty or invalid;"));
	if (NULL == _lp_sz_dest || 0 == ::_tcslen(_lp_sz_dest)) return ((m_error = E_INVALIDARG) = _T("Unzip path is empty or invalid;"));

	HRESULT hr_ = m_error;

	CGenericFolder dest_(_lp_sz_dest);
	if (dest_.Path().IsExist()) {
		hr_ = dest_.Delete();
		if (FAILED(hr_))
			return (m_error = dest_.Error());
	}
	hr_ = dest_.Create();
	if (FAILED(hr_))
		return (m_error = dest_.Error());

	CCurrentFolder curr_;
	curr_ = dest_.Path();

	_sink.ICrx_OnUnpackStart();

	HZIP hz = ::OpenZip(CAtlString(_lp_sz_zip).GetBuffer(), 0, ZIP_FILENAME);
	if ( hz == NULL )
		return ((m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("Cannot open the archive;"));

	ZIPENTRYW zentry_ = {0};
	ZRESULT  zresult_ = ::GetZipItem(hz, -1, &zentry_);

	do {
		if (ZR_OK == zresult_) {
			const INT total_ = zentry_.index;
			for ( INT i_ = 0; i_ < total_; i_ ++) {
				zresult_ = ::GetZipItem(hz, i_, &zentry_);
				if (ZR_OK != zresult_) {
					(m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("Invalid archive entry;");
					break;
				}
				if (zentry_.attr & FILE_ATTRIBUTE_DIRECTORY) // does not work;
					continue;

				CAtlString cs_name(zentry_.name);
				if (cs_name.GetLength() - 1 == cs_name.ReverseFind('/'))
					continue;

				_sink.ICrx_OnUnpackProgress(
					static_cast<DWORD>(i_ + 1),
					static_cast<DWORD>(total_),
					cs_name
					);
				zresult_ = ::UnzipItem(hz , i_ ,zentry_.name, 0 , ZIP_FILENAME);
				if (ZR_OK != zresult_) {
					CAtlString cs_error;
					cs_error.Format(
						_T("Cannot unpack file %s"),
						zentry_.name
					);
					(m_error = E_UNEXPECTED) = (LPCTSTR) cs_error; break;
				}
			}
		}
		else {
			(m_error = __DwordToHresult(ERROR_INVALID_DATA)) = _T("Cannot retrieve an archive content properties;");
		}
	} while (false == true);

	if (hz)
		::CloseZip(hz);

	if (m_error.Is() == true) {
		_sink.ICrx_OnUpdateError(m_error);
	}
	else {
		_sink.ICrx_OnUnpackFinish();
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////