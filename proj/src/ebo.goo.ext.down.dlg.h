#ifndef _EBOGOOEXTDOWNDLG_H_15346C14_14E2_4CA0_91CF_30C458F631E9_INCLUDED
#define _EBOGOOEXTDOWNDLG_H_15346C14_14E2_4CA0_91CF_30C458F631E9_INCLUDED
/*
	Created by Tech_Dog (ebontrop@gmail.com) on 11-Apr-2017 at 3:30:14a, UTC+7, Phuket, Rawai, Tuesday;
	This is File Guardian installer download UI dialog class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google Chrome extension project on 6-Oct-2019 at 1:15:19a, UTC+7, Novosibirsk, Tulenina, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.net.single.thread.downloader.h"

namespace ebo { namespace out { namespace ext { namespace dialogs
{
	using shared::sys_core::CError;
	using shared::net::INetDownloaderCallback;
	using shared::net::CNetSingleThreadDownloader;

	class CDownloadDlg
	{
	private:
		class CDownloadDlgImpl :
			public  ::ATL::CDialogImpl<CDownloadDlgImpl>,
			public  INetDownloaderCallback
		{
			typedef ::ATL::CDialogImpl<CDownloadDlgImpl> TBaseDlg;
			friend class CDownloadDlg;
		private:
			CNetSingleThreadDownloader m_loader;
			WTL::CProgressBarCtrl      m_progress;
			CWindow                    m_label;
			INT_PTR                    m_timer;
			volatile bool              m_interrupted;
			CAtlString                 m_source; // source URL
			CAtlString                 m_target; // destination file full path
			CError                     m_error;  // download process error object
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CDownloadDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy   )
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCommand)
				MESSAGE_HANDLER     (WM_TIMER     ,   OnTimerEvent)
				COMMAND_ID_HANDLER  (IDCANCEL     ,   OnDismiss   )
			END_MSG_MAP()
		public:
			CDownloadDlgImpl(const CAtlString& _source, const CAtlString& _target);
			~CDownloadDlgImpl(void);
		private: // message handlers
			LRESULT OnDestroy   (UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDismiss   (WORD wNotifyCode, WORD wID     , HWND hControl, BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnTimerEvent(UINT uMsg       , WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		private: // INetDownloaderCallback
			virtual bool    INetDownloader_OnCanContinue (void) override sealed;
			virtual void    INetDownloader_OnBegin       (void) override sealed;
			virtual void    INetDownloader_OnFinish      (void) override sealed;
			virtual void    INetDownloader_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal) override sealed;
			virtual void    INetDownloader_OnError     (CError) override sealed;
			virtual void    INetDownloader_OnInterrupt   (void) override sealed;
		};
	private:
		CDownloadDlgImpl   m_dlg;
	public:
		 CDownloadDlg(const CAtlString& _source, const CAtlString& _target);
		~CDownloadDlg(void);
	public:
		HRESULT    DoModal(const HWND hParent);
		TErrorRef  Error  (void) const;
	private:
		CDownloadDlg (const CDownloadDlg&);
		CDownloadDlg& operator= (const CDownloadDlg&);
	};
}}}}

#endif/*_EBOGOOEXTDOWNDLG_H_15346C14_14E2_4CA0_91CF_30C458F631E9_INCLUDED*/