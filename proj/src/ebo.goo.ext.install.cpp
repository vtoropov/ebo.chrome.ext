/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Oct-2019 at 8:19:39p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Pack shared library generic installation wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.goo.ext.install.h"

using namespace ebo::out::ext;

/////////////////////////////////////////////////////////////////////////////

CInst_Info:: CInst_Info(void) {}
CInst_Info::~CInst_Info(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CAtlString&  CInst_Info::Name  (void) const { return m_name; }
CAtlString&  CInst_Info::Name  (void)       { return m_name; }
const
CAtlString&  CInst_Info::Path  (void) const { return m_path; }
CAtlString&  CInst_Info::Path  (void)       { return m_path; }
const
CAtlString&  CInst_Info::Target(void) const { return m_dest; }
CAtlString&  CInst_Info::Target(void)       { return m_dest; }

/////////////////////////////////////////////////////////////////////////////

CInstaller:: CInstaller (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CInstaller::~CInstaller (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CInstaller::Copy  (LPCTSTR _lp_sz_src, LPCTSTR _lp_sz_to, const bool _b_override) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_src || 0 == ::_tcslen(_lp_sz_src))
		return (m_error = E_INVALIDARG) = _T("Source path cannot be empty;");

	if (NULL == _lp_sz_to  || 0 == ::_tcslen(_lp_sz_to) )
		return (m_error = E_INVALIDARG) = _T("Destination path cannot be empty;");

	CGenericFile  src_(_lp_sz_src);
	HRESULT hr_ = src_.CopyTo(_lp_sz_to, _b_override);
	if (FAILED(hr_)) {
		return (m_error = src_.Error());
	}

	return m_error;
}

HRESULT     CInstaller::Find  (LPCTSTR _lp_sz_exe, CAtlString& _path) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_exe || 0 == ::_tcslen(_lp_sz_exe))
		return (m_error = E_INVALIDARG) = _T("Executable name cannot be empty;");

	CAtlString cs_key;
	cs_key.Format(
		_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\%s"), _lp_sz_exe
	);

	CRegistryStg reg_stg(HKEY_LOCAL_MACHINE, CRegOptions::eDoNotModifyPath);
	HRESULT hr_ = reg_stg.Load(
		(LPCTSTR)cs_key, _T(""), m_info.Target()
		);
	if (FAILED(hr_))
		return (m_error = reg_stg.Error());
	else
		_path = m_info.Target();

	hr_ = reg_stg.Load(
		(LPCTSTR)cs_key, _T("Path"), m_info.Path()
	);

	m_info.Name() = _lp_sz_exe;

	return m_error;
}
TErrorRef   CInstaller::Error (void) const { return m_error; }
const
CInst_Info& CInstaller::Info  (void) const { return m_info ; }

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////