#ifndef _EBOGOOEXTRUNNER_H_71CA4F2C_7E53_4532_A673_AF62A18AB054_INCLUDED
#define _EBOGOOEXTRUNNER_H_71CA4F2C_7E53_4532_A673_AF62A18AB054_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Oct-2019 at 10:03:45p, UTC+7, Novosibirsk, Tulenina, Tuesday;
	This is Ebo Pack shared library generic application runner interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.fs.gen.file.h"
#include "shared.sys.proc.h"

#include "ebo.goo.ext.install.h"

namespace ebo { namespace out { namespace ext {

	using shared::sys_core::CError;
	using shared::sys_core::TProcessList;

	class CRunner {
	protected:
		mutable
		CError       m_error;
		CAtlString   m_args ;  // command line argument(s) string;

	public:
		 CRunner (void);
		~CRunner (void);

	public:
		LPCTSTR     Args   (void) const;
		VOID        Args   (LPCTSTR _lp_sz_crx)                     ;   // creates an argument(s) of command line;
		TErrorRef   Error  (void) const;
		HRESULT     Run    (const CInst_Info& )                     ;   // when chrome app is copied to file with another name, running process still has original name of chrome;
		HRESULT     Run    (LPCTSTR _lp_sz_exe)                     ;   // command line is empty or is already created via params;
		HRESULT     Run    (LPCTSTR _lp_sz_exe, LPCTSTR _lp_sz_crx) ;   // applies crx file to command line of chrome application;
		bool        Running(LPCTSTR _lp_sz_exe) const               ;   // finds all running instances by executable name provided;
	};

}}}

#endif/*_EBOGOOEXTRUNNER_H_71CA4F2C_7E53_4532_A673_AF62A18AB054_INCLUDED*/