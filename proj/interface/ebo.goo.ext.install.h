#ifndef _EBOGOOEXTINSTALL_H_A4A91F2A_7D2E_4707_B671_0381EA5BDED7_INCLUDED
#define _EBOGOOEXTINSTALL_H_A4A91F2A_7D2E_4707_B671_0381EA5BDED7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Oct-2019 at 8:10:27p, UTC+7, Novosibirsk, Tulenina, Friday;
	This is Ebo Pack shared library generic installation wrapper interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.reg.hive.defs.h"
#include "shared.reg.hive.enum.h"
#include "shared.reg.hive.stg.h"
#include "shared.fs.gen.file.h"

namespace ebo { namespace out { namespace ext {

	using shared::sys_core::CError;
	using shared::registry::CRegKeyEnum;
	using shared::registry::CRegOptions;
	using shared::registry::CRegistryStg;
	using shared::ntfs::CGenericFile;

	class CInst_Info {
	protected:
		CAtlString   m_name;   // a name of an executable being found;
		CAtlString   m_path;   // a path (absolute) of installation  (no exe name);
		CAtlString   m_dest;   // a complete installation path (folder + exe name);

	public:
		 CInst_Info (void) ;
		~CInst_Info (void) ;

	public:
		const
		CAtlString&  Name  (void) const;
		CAtlString&  Name  (void)      ;
		const
		CAtlString&  Path  (void) const;
		CAtlString&  Path  (void)      ;
		const
		CAtlString&  Target(void) const;   // absolute path that contains installation folder and executable name;
		CAtlString&  Target(void)      ;
	};

	class CInstaller {
	protected:
		CError      m_error;
		CInst_Info  m_info ;

	public:
		 CInstaller (void) ;
		~CInstaller (void) ;

	public:
		HRESULT     Copy  (LPCTSTR _lp_sz_src, LPCTSTR _lp_sz_to , const bool _b_override) ; // copies source file to destination provided;
		HRESULT     Find  (LPCTSTR _lp_sz_exe, CAtlString& _path ) ;      // finds an installation path of the app executable name in the registry;
		TErrorRef   Error (void) const;
		const
		CInst_Info& Info  (void) const;
	};

}}}


#endif/*_EBOGOOEXTINSTALL_H_A4A91F2A_7D2E_4707_B671_0381EA5BDED7_INCLUDED*/