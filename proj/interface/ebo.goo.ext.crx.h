#ifndef _EBOGOOEXTCRX_H_16A38F42_0493_4D6E_B7E7_829E2871D0DE_INCLUDED
#define _EBOGOOEXTCRX_H_16A38F42_0493_4D6E_B7E7_829E2871D0DE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Oct-2019 at 4:58:22p, UTC+7, Novosibirsk, Tulenina, Monday;
	This is Ebo Pack shared library generic CRX wrapper interface declaration file.
*/
#include "XUnzip.h"
#include "shared.gen.sys.err.h"

namespace ebo { namespace out { namespace ext {

	using shared::sys_core::CError;

	interface ICrxCallback
	{
		virtual void       ICrx_OnUnpackFinish  (void  )    PURE;
		virtual void       ICrx_OnUnpackProgress(const DWORD dwIndex, const DWORD dwTotal, CAtlString _file) PURE;
		virtual void       ICrx_OnUnpackStart   (void  )    PURE;
		virtual void       ICrx_OnUpdateError   (CError)    PURE;
	};

	class CCrx {
	private:
		CError     m_error;

	public:
		 CCrx (void);
		~CCrx (void);

	public:
		TErrorRef    Error  (void) const;
		HRESULT      Unpack (LPCTSTR _lp_sz_zip, LPCTSTR _lp_sz_dest, ICrxCallback&); // unpacks ZIP archive that contains CRX inside;
	};

}}}

#endif/*_EBOGOOEXTCRX_H_16A38F42_0493_4D6E_B7E7_829E2871D0DE_INCLUDED*/