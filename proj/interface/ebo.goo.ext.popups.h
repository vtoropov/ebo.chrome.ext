#ifndef _EBOGOOEXTPOPUPS_H_BB242C72_C6F7_482A_ABB7_FA83E64493B7_INCLUDED
#define _EBOGOOEXTPOPUPS_H_BB242C72_C6F7_482A_ABB7_FA83E64493B7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Oct-2019 at 7:13:45p, UTC+7, Novosibirsk, Tulenina, Thursday;
	This is Ebo Pack shared library Google Chrome popup message handler interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.sys.tpl.h"
#include "shared.sys.proc.h"
#include "ebo.goo.ext.install.h"

namespace ebo { namespace out { namespace ext {

	using shared::sys_core::CError;
	using shared::sys_core::TProcessList;
	using shared::sys_core::TWindows;
	using shared::sys_core::CThreadBase;

	/*
		Match method uses:
		1) Find() - is case sensetive; if not found,
		2) CompareNoCase();
	*/
	class CFrame {
	protected:
		CWindow     m_wnd;   // a window of the frame;

	public:
		 CFrame (const HWND h_wnd = NULL);
		~CFrame (void);

	public:
		HRESULT     Close (void)      ;  // tries to close the popup;
		const bool  Match (LPCTSTR _lp_sz_cap) const;  // checks a caption of the frame for string provided;
		const
		CWindow&    Window(void) const;
		CWindow&    Window(void)      ;

	public:
		CFrame& operator = (const HWND);  // assings a window handle to the frame;
	};

	typedef ::std::vector<CFrame>  TFrames;

	class CFrames {
	private:
		mutable
		CError      m_error ;
		TFrames     m_frames;

	public:
		 CFrames (void);
		~CFrames (void);

	public:
		HRESULT     Append (const TWindows& _handles);             // adds a list of existing windows of a particular process;
		HRESULT     Close  (const INT _n_ndx)        ;             // closes the frame by index specified;
		HRESULT     Create (const TProcessList& _processes);       // creates a list of frames of processes provided;
		TErrorRef   Error  (void) const;
		INT         IsFound(LPCTSTR lp_sz_cap) const;              // finds a frame with specified caption; if not found, returns -1;
		HRESULT     Update (LPCTSTR lp_sz_exe)      ;              // updates frame list for executable instances;

	public:
		CFrames&    operator+=(const TWindows& _handles);          // appends a list of existing popup windows;
		CFrames&    operator<<(const TProcessList& _processes);    // creates frame list for all processes provided
	};

	class CPopups {
		// based on:
		// https://sudonull.com/posts/587-Chrome-reverse-and-installation-of-extensions
	protected:
		mutable
		CError      m_error ;
		CFrames     m_frames;

	public:
		 CPopups (void) ;
		~CPopups (void) ;

	public:
		HRESULT     Block  (const INT _i_ndx );                // closes a popup frame by the index specified;
		HRESULT     Block  (LPCTSTR lp_sz_cap);                // looks for a popup by its caption and closes it; [not effective];
		TErrorRef   Error  (void) const;
		const
		CFrames&    Frames (void) const;
		CFrames&    Frames (void)      ;
	};

	class CPopup_Async : public CThreadBase, public CPopups {
	                    typedef CThreadBase TThread;
	                    typedef CPopups     TPopups;
	private:
		volatile
		DWORD       m_msec;    // msecs for waiting popup appearance;
		CInst_Info  m_info;
		CAtlString  m_suppress;// caption of a popup that must be suppressed;

	public:
		 CPopup_Async (void);
		~CPopup_Async (void);

	public:
		HRESULT     Suppress (LPCTSTR lp_sz_cap, const CInst_Info& _info, const DWORD _wait);
	private:
		virtual
		VOID        ThreadFunction(VOID);
	};

}}}

#endif/*_EBOGOOEXTPOPUPS_H_BB242C72_C6F7_482A_ABB7_FA83E64493B7_INCLUDED*/