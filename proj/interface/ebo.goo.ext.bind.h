#ifndef _EBOGOOEXTBIND_H_703D34CA_90B5_42D9_9E8A_720FDC7C6EE1_INCLUDED
#define _EBOGOOEXTBIND_H_703D34CA_90B5_42D9_9E8A_720FDC7C6EE1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Oct-2019 at 9:32:44p, UTC+7, Novosibirsk, Tulenina, Saturday;
	This is Ebo Pack shared library generic dynamic binding interface declaration file.
*/
#include <iphlpapi.h>
//
//  Macros is borrowed from Apache APR project:
//     https://apr.apache.org/compiling_win32.html;
//
//  Compiler error appears due to undefined NETIOAPI_API_ type in arp_arch_misc.h (line 500);
//  The header file above must be included for getting compilation process success;
//  Actually this type is defined in netioapi.h that is included in cases when NTDDI_VERSION >= NTDDI_VISTA,
//  otherwise, there is no chance to get it compiled under Windows XP;
//

namespace ebo { namespace out { namespace ext {

	typedef enum {
		e_kernel = 0,
		e_advapi = 1,
		e_socket = 2,
		e_ws2_32 = 3,
		e_shell  = 4,
		e_user32 = 5,
		e_helper = 6,
	}   e_lib_token;

	LPCTSTR bind_token_to_lib (const e_lib_token);
	FARPROC bind_load_dll_func(const e_lib_token, LPCSTR _fn_name, const INT _ordinal);


#define __ebo_dyna_bind(_token, _rettype, _calltype, _fn, _ordinal, _args, _names)  \
	typedef _rettype (_calltype *dyna_bind_fpt_##_fn)   _args;                      \
	                                                                                \
	static dyna_bind_fpt_##_fn   dyna_bind_pfn_##_fn  =  NULL;                      \
	                                                                                \
	static INT dyna_winapi_counter_##_fn = 0;                                       \
	                                                                                \
	static __inline INT dyna_bind_ld_##_fn(void) {                                  \
		if (dyna_bind_pfn_##_fn)                                                    \
			return 1;                                                               \
		if (dyna_winapi_counter_##_fn ++)                                           \
			return 0;                                                               \
		if (dyna_bind_pfn_##_fn == NULL) {                                          \
		    dyna_bind_pfn_##_fn = (dyna_bind_fpt_##_fn)                             \
			bind_load_dll_func (_token, #_fn, _ordinal);                            \
		}                                                                           \
		if (dyna_bind_pfn_##_fn == NULL)                                            \
			return 0;                                                               \
		else                                                                        \
			return 1;                                                               \
	}                                                                               \
	                                                                                \
	static __inline _rettype dyna_bind_##_fn _args {                                \
		if (dyna_bind_ld_##_fn())                                                   \
			return (*(dyna_bind_pfn_##_fn)) _names;                                 \
		else {                                                                      \
			::SetLastError(ERROR_INVALID_FUNCTION);                                 \
			return 0;                                                               \
		}                                                                           \
	};

#pragma region __test

#ifdef if_indextoname
#undef if_indextoname
#endif

	__ebo_dyna_bind(
		e_lib_token::e_helper, PCHAR, NETIOAPI_API_, if_indextoname, 0,
		(NET_IFINDEX InterfaceIndex , PCHAR InterfaceName ),
		(            InterfaceIndex ,       InterfaceName));
#define if_indextoname dyna_bind_if_indextoname

#pragma endregion

}}}

#endif/*_EBOGOOEXTBIND_H_703D34CA_90B5_42D9_9E8A_720FDC7C6EE1_INCLUDED*/