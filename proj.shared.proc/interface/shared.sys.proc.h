#ifndef _SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED
#define _SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2015 at 5:44:54p, UTC+3, Taganrog, Sunday;
	This is Shared Lite Library Generic Process class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 10-Oct-2019 at 9:34:21a, UTC+7, Novosibirsk, Tulenina, Thursday;
*/
#include <Psapi.h>
#pragma comment(lib, "Psapi.lib")

#include "shared.gen.sys.err.h"

namespace shared { namespace sys_core {

	using shared::sys_core::CError;

	typedef ::std::vector<HWND> TWindows;
	class CProcess
	{
	private:
		DWORD         m_dwId;
		CAtlString    m_module;
		CAtlString    m_desc;
		CAtlString    m_name;
		TWindows      m_wins;   // all windows that are associated with this process (including child windows);
	public:
		 CProcess(void);
		 CProcess(const DWORD dwId);
		~CProcess(void);
	public:
		LPCTSTR       Desc    (void) const ;
		HRESULT       Desc    (LPCTSTR)    ;
		DWORD         ExitCode(void) const ;
		DWORD         Id      (void) const ;
		HRESULT       Id      (const DWORD);
		LPCTSTR       Module  (void) const ;
		HRESULT       Module  (LPCTSTR)    ;    // sets full path of the module
		LPCTSTR       Name    (void)  const;
		HRESULT       Name    (LPCTSTR)    ;    // sets the executable name (without path), i.e. file name;
		HRESULT       Run     (LPCTSTR pszModule, LPCTSTR pszCmdLine, const bool bWithAdminRights);
		bool          Running (void)  const;
		const
		TWindows&     Windows (void)  const;
		TWindows&     Windows (void)       ;
	};

	typedef ::std::vector<CProcess> TProcessList;

	class CProcessEnum {
	private:
		CError        m_error;
		TProcessList  m_proc_list;
	public:
		 CProcessEnum(void);
		~CProcessEnum(void);
	public:
		HRESULT       Enumerate(void);
		TErrorRef     Error(void) const;
		CProcess      GetResourceLock(LPCTSTR pszFile, LPCTSTR pszFilter);
		const
		TProcessList& List (void)const;
		HRESULT       TakeSnapshot(void); // assign windows flag is used for getting all windows with a process;
	};

	class CProcLookup {
	private:
		mutable
		CError        m_error;

	public:
		 CProcLookup (void);
		~CProcLookup (void);

	public:
		TErrorRef     Error(void) const;
		const
		TProcessList  Find (LPCTSTR lp_sz_exe_name, const bool b_assign_wnds = false) const; // finds running process instances by specified exe name;
	}; 
}}

#endif/*_SHAREDLITEGENERICPROCESS_H_0B4952EA_A871_4e85_86ED_838BFBCE4316_INCLUDED*/