#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Tulenina, Sunday;
	This is FIX Engine shared library configuration command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 11:52:11a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to serial port data project on 21-May-2019 at 7:12:03p, UTC+7, Phuket, Rawai, Tuesday;
	Adopted to shared memory project on 8-Jun-2019 at 1:34:01a, UTC+7, Novosibirsk, Tulenina, Saturday;
*/

namespace shared { namespace common {

	typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TCmdLineArgs;

	class CCommandLine {
	private:
		CAtlString   m_module_path;
		TCmdLineArgs m_args;
	public:
		CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_val);
		CAtlString   Arg   (LPCTSTR _lp_sz_nm) const;
		LONG         Arg   (LPCTSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void)const;                                  // returns a copy of command line argument collection
		VOID         Clear (void)     ;
		INT          Count (void)const;
		bool         Has   (LPCTSTR pszArgName)const;
		CAtlString   Path  (void)const;                                  // returns executable absolute path;
		CAtlString   ToString  (LPCTSTR _lp_sz_sep = NULL) const;

	public:
		operator LPCTSTR (void) const;

	public:
		bool operator==(LPCTSTR pszArgName) const;
	};
}}

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/