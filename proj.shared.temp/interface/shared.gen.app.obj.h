#ifndef _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
#define _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jun-2016 at 1:28:28p, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library Generic Application class declaration file.
*/
#include <Shellapi.h>
#include <map>
#pragma comment(lib, "version.lib")

namespace shared { namespace user32
{
	typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TCmdLineArguments;

	class CApplication
	{
	public:
		class CObject
		{
		private:
			HANDLE              m_mutex;
			::ATL::CAtlString   m_mutex_name;
			DWORD               m_proc_state;
			CONST CApplication& m_app_obj_ref;
		public:
			explicit CObject(CONST CApplication&);
			~CObject(VOID);
		public:
			bool                IsSingleton(void) const;
			HRESULT             RegisterSingleton(LPCTSTR pMutexName);
			HRESULT             UnregisterSingleton(VOID);
		};
		class CVersion
		{
		private:
			mutable HRESULT     m_hResult;     // last result
			LPVOID              m_pVerInfo;    // file version info pointer
		public:
			CVersion(void);
			CVersion(LPCTSTR pszModulePath);
			~CVersion(void);
		public:
			::ATL::CAtlString   Comments(void)           const;
			::ATL::CAtlString   CompanyName(void)        const;
			::ATL::CAtlString   CopyRight(void)          const;
			::ATL::CAtlString   CustomValue(LPCTSTR lpszValueName)const;
			::ATL::CAtlString   FileDescription(void)    const;
			::ATL::CAtlString   FileVersion(void)        const;
			::ATL::CAtlString   FileVersionFixed(void)   const;
			HRESULT             GetLastResult(void)      const;
			::ATL::CAtlString   InternalName(void)       const;
			::ATL::CAtlString   OriginalFileName(void)   const;
			::ATL::CAtlString   ProductName(void)        const;
			::ATL::CAtlString   ProductVersion(void)     const;
			::ATL::CAtlString   ProductVersionFixed(void)const;
		};

		class CCommandLine
		{
		private:
			::ATL::CAtlString   m_module_full_path;
			TCmdLineArguments   m_args;
		public:
			CCommandLine(void);
			~CCommandLine(void);
		public:
#if (1)
			HRESULT             Append   (LPCTSTR pszName, LPCTSTR lpszValue);
#endif
			::ATL::CAtlString   Argument (LPCTSTR pszName)const;
			LONG                Argument (LPCTSTR pszName, const LONG _def_val)const;
			TCmdLineArguments   Arguments(void)const;                                  // returns a copy of command line argument collection
			VOID                Clear(void)     ;
			INT                 Count(void)const;
			bool                Has(LPCTSTR pszArgName)const;
			::ATL::CAtlString   ModuleFullPath(void)const;
			::ATL::CAtlString   ToString(void)const;
		};
	private:
		HRESULT                 m_hResult;     // last result
		mutable 
		::ATL::CAtlString       m_app_name;
		CVersion                m_version;
		CObject                 m_object;
		CCommandLine            m_cmd_line;
	public:
		CApplication(void);
		~CApplication(void);
	public:
		const CCommandLine&     CommandLine(void) const;
#if defined(_DEBUG)
		      CCommandLine&     CommandLine(void);
#endif
		::ATL::CAtlString       GetFileName(const bool bNoExtension = false) const;
		HRESULT                 GetLastResult(void) const;
		LPCTSTR                 GetName(void) const;
		HRESULT                 GetPath(::ATL::CAtlString& __in_out_ref) const;
		HRESULT                 GetPathFromAppFolder(LPCTSTR lpszPattern, ::ATL::CAtlString& __in_out_ref) const;
		const CObject&          Instance(VOID)const;
		CObject&                Instance(VOID);
		const CVersion&         Version(VOID)const;
	public:
		static HRESULT          GetFullPath(::ATL::CAtlString&);
		static bool             Is64bit(VOID);
		static bool             IsRelatedToAppFolder(LPCTSTR pPath);
	private:
		CApplication(const CApplication&);
		CApplication& operator= (const CApplication&);
	};

	CApplication&  GetAppObjectRef(void); // gets the static object reference; it is used in a client of version 2
}}

typedef shared::user32::CApplication::CCommandLine TCommandLine;

#endif/*_SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED*/