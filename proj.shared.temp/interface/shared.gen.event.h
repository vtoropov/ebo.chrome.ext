#ifndef _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
#define _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Jul-2013 at 10:13:32pm, GMT+3, Taganrog, Sunday;
	This is Shared Lite Library Generic Event class declaration file.
*/
namespace shared { namespace runnable
{
	interface IGenericEventNotify
	{
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) PURE;
		virtual HRESULT  GenericEvent_OnNotify(const LONG n_evt_id, const _variant_t v_data) {n_evt_id; v_data; return E_NOTIMPL; }
	};
	class CMarshaller
	{
	protected:
		HANDLE           m_handler;
	public:
		CMarshaller(IGenericEventNotify&, const _variant_t& v_evt_id);
		virtual ~CMarshaller(void);
	public:
		virtual HRESULT  Create(void);
		virtual HRESULT  Destroy(void);
		virtual HWND     GetHandle_Safe(void) const;
	private:
		CMarshaller(const CMarshaller&);
		CMarshaller& operator= (const CMarshaller&);
	public:
		virtual HRESULT  Fire(const bool bAsynch = true);
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch = true);
	public:
		virtual HRESULT  Fire2(void);
	};

	class CWaitCounter
	{
	public:
		enum{
			eInfinite = (DWORD)-1
		};
	protected:
		DWORD     m_nTimeSlice;    // in milliseconds
		DWORD     m_nCurrent;      // current time
		DWORD     m_nTimeFrame;    // total time to wait for
	public:
		CWaitCounter(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CWaitCounter(void);
	public:
		virtual bool IsElapsed(void) const;
		virtual bool IsReset  (void) const;
		virtual VOID Reset(const DWORD nTimeSlice = eInfinite, const DWORD nTimeFrame = eInfinite);
		virtual VOID Wait (void);
	};
}}

#endif/*_SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED*/