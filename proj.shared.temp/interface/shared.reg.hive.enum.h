#ifndef _SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED
#define _SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jun-2018 at 4:57:32p, UTC+7, Phuket, Rawai, Friday;
	This is Shared Library registry entry enumerator interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 30-Jul-2018 at 4:18:21p, UTC+7, Novosibirsk, Rodniki, Tulenina, Monday;
*/
#include "shared.reg.hive.defs.h"

namespace shared { namespace registry {

	typedef ::std::vector<CAtlString> TRegKeyEnum;

	class CRegKeyEnum : public CRegistryBase {
	                   typedef CRegistryBase TBase;
	public:
		 CRegKeyEnum(const HKEY hRoot, const CRegOptions = CRegOptions::eNone);
		~CRegKeyEnum(void);
	public:
		TRegKeyEnum Enumerate(LPCTSTR lpszFolder);
	};

}}

#endif/*_SHAREDREGISTRYENUM_H_30220D21_3178_4479_8091_0D217F41DF52_INCLUDED*/