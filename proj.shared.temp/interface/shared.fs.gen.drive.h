#ifndef _SHAREDFSGENERICDRIVE_H_330BBE1C_61D8_4e22_8A93_B9C385504275_INCLUDED
#define _SHAREDFSGENERICDRIVE_H_330BBE1C_61D8_4e22_8A93_B9C385504275_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2016 at 12:55:31pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared NTFS library generic drive wrapper class(es) interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 9-Jul-2018 at 6:01:07p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.fs.defs.h"

#include <Dbt.h>

namespace shared { namespace ntfs
{
	typedef ::std::vector<DWORD> TDriveTypes;

	class CGenericDriveType
	{
	public:
		enum _e{
			eUnknown    = DRIVE_UNKNOWN    ,  // the drive type cannot be determined;
			eNoRoot     = DRIVE_NO_ROOT_DIR,  // root path is invalid; for example, there is no volume is mounted at the path;
			eRemovable  = DRIVE_REMOVABLE  ,  // the drive has removable media; for example, a floppy drive, thumb drive, or flash card reader;
			eFixed      = DRIVE_FIXED      ,  // the drive has fixed media; for example, a hard drive or flash drive;
			eRemote     = DRIVE_REMOTE     ,  // the drive is a remote (network) drive;
			eOptical    = DRIVE_CDROM      ,  // the drive is a CD-ROM drive;
			eMemory     = DRIVE_RAMDISK    ,  // the drive is a RAM disk;
		};
	public:
		static TDriveTypes    GetAllTypes(void);
	};

	class CGenericDriveEnum;
	class CGenericDrive
	{
		friend class CGenericDriveEnum;
	private:
		CGenericDriveType::_e m_type;
		CAtlString            m_letter;
		CAtlString            m_dos_name;  // DOS device name;
	public:
		CGenericDrive(void);
		CGenericDrive(LPCTSTR lpszLetter);
	public:
		LPCTSTR               DosName(void)const;
		CAtlString            Letter(void)const;
		CAtlString            Path(void)const;
		CAtlString            Pattern(void)const;
		CGenericDriveType::_e Type(void)const;
		VOID                  Type(const UINT);
	};


	typedef ::std::vector<CGenericDrive> TLogicalDrives;
	typedef ::std::vector<CGenericDriveType::_e> TDriveFilter;

	class CGenericDriveEnum
	{
	private:
		CError            m_error;
	public:
		CGenericDriveEnum(void);
	public:
		TLogicalDrives    Enumerate(const TDriveTypes& _eUnitMask);
		TDriveList        Enumerate(const TDriveFilter& _filter);
		TErrorRef         Error(void)const;
	public:
		static
		CAtlString        ConvertLocalToUNC(LPCTSTR lpszDosPath, const TLogicalDrives&);
		static
		CAtlString        ConvertUNCToLocal(LPCTSTR lpszUncPath, const TLogicalDrives&);
	};

	class CGenericDriveEvent
	{
	public:
		enum Id{
			eUnknown       = 0x0,
			ePlugged       = DBT_DEVICEARRIVAL    ,
			eRemoveQueried = DBT_DEVICEQUERYREMOVE,       // request is sent to remove a device, it may fail;
			eRemoveAborted = DBT_DEVICEQUERYREMOVEFAILED, // removal cancelled by a user or aborted by hardware;
			eRemovePending = DBT_DEVICEREMOVEPENDING    , // about to remove, a device still be available;
			eRemoveDone    = DBT_DEVICEREMOVECOMPLETE   , // device remove is complete, a device has gone;
			eTypeSpecific  = DBT_DEVICETYPESPECIFIC     , // device type specific event
		};
	private:
		Id                m_id;
	public:
		CGenericDriveEvent(void);
		CGenericDriveEvent(const CGenericDriveEvent::Id);
	public:
		CGenericDriveEvent& operator= (const CGenericDriveEvent::Id);
	public:
		CAtlString        Title(void)const;
	};

	interface IGenericDeviceCallback
	{
		virtual VOID      IGenericDriveCallback_OnNotify(const CGenericDriveEvent::Id, const CGenericDrive) PURE;
	};

	class CGenericDeviceNotifier
	{
	private:
		HANDLE            m_handler;       // notification message handler;
		HANDLE            m_notify;        // notification subscription handle;
		bool              m_bInitialized;
		CError            m_error;
	public:
		CGenericDeviceNotifier(IGenericDeviceCallback&);
		~CGenericDeviceNotifier(void);
	public:
		TErrorRef         Error(void)const;
		HRESULT           Initialize(void);
		bool              IsInitialized(void)const;
		HRESULT           Terminate(void);
	private:
		CGenericDeviceNotifier(const CGenericDeviceNotifier&);
		CGenericDeviceNotifier& operator= (const CGenericDeviceNotifier&);
	};
}}

#endif/*_SHAREDFSGENERICDRIVE_H_330BBE1C_61D8_4e22_8A93_B9C385504275_INCLUDED*/